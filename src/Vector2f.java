import java.io.Serializable;

public class Vector2f implements Serializable {
    private float x;
    private float y;


    public Vector2f() { x = 0; y = 0; }

    public Vector2f(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public Vector2f(Vector2f vector) {
        x = vector.x;
        y = vector.y;
    }

    public void add(Vector2f vector) {
        add(vector.x, vector.y);
    }

    public void setX(float x) {
        this.x = x;
    }

    public void setY(float y) {
        this.y = y;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public void add(float x, float y) {
        this.x += x;
        this.y += y;
    }

    public void subtract(Vector2f vector) {
        add(-vector.x, -vector.y);
    }


    public void multiply(float f) {
        x *= f;
        y *= f;
    }

    public void divide(float f) {
        if (f == 0)
            return;
        x /= f;
        y /= f;
    }

//    public float distance(Vector vector) {
//        float res = (float)(Math.pow(x-vector.x, 2) + Math.pow(y-vector.y, 2));
//        return (float)Math.sqrt(res);
//
//}
    public float length() {
        return (float)Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
    }

    public static float distance(Vector2f v1, Vector2f v2) {
        return (float)Math.sqrt(Math.pow(v1.x - v2.x, 2) + Math.pow(v1.y - v2.y, 2));
    }

    //return new Vector2f that is разность
    public static Vector2f getDifference(Vector2f v1, Vector2f v2) {
        return new Vector2f(v1.x - v2.x, v1.y - v2.y);
    }

    public static Vector2f getQuotient(Vector2f v, float value) {
        return new Vector2f(v.x / value, v.y / value);
    }
}
