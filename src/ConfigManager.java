import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

public class ConfigManager {
    private File configFile;
    private Habitat habitat;

    ConfigManager(File configFile, Habitat habitat) {
        this.configFile = configFile;
        this.habitat = habitat;
    }

    public void read() {
        try (Scanner scanner = new Scanner(configFile)) {
            scanner.useLocale(Locale.US);
            int adultPeriod = readIntValue(scanner);
            habitat.setPeriodOfAdult(adultPeriod);
            int nestlingPeriod = readIntValue(scanner);
            habitat.setPeriodOfNestling(nestlingPeriod);

            float adultChance = readFloatValue(scanner);
            habitat.setChanceOfAdult(adultChance);
            int nestlingPercent = readIntValue(scanner);
            habitat.setSpecifiedPercent(nestlingPercent);

            int adultLifetime = readIntValue(scanner);
            AdultBird.setLifetime(adultLifetime);
            int nestlingLifetime = readIntValue(scanner);
            Nestling.setLifetime(nestlingLifetime);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
    private int readIntValue(Scanner scanner) {
        scanner.next();
        scanner.next();
        scanner.next();
        if (scanner.hasNextInt())
            return scanner.nextInt();

        return -1;
    }
    private float readFloatValue(Scanner scanner) {
        scanner.next();
        scanner.next();
        scanner.next();
        if (scanner.hasNextFloat())
            return scanner.nextFloat();

        return -1;
    }
    public void write() {
        try (PrintWriter writer = new PrintWriter(configFile)) {
            int value = habitat.getPeriodOfAdult();
            float fvalue = habitat.getChanceOfAdult();
            writeIntValue(writer,"period of adult ", value);
            value = habitat.getPeriodOfNestling();
            writeIntValue(writer, "period of nestling ", value);
            writeFloatValue(writer, "chances of adultbirth ", fvalue);
            value = habitat.getSpecifiedPercent();
            writeIntValue(writer, "percent of nestlingbirth ", value);
            value = AdultBird.getLifeTime();
            writeIntValue(writer,"lifetime of adult ", value);
            value = Nestling.getLifeTime();
            writeIntValue(writer, "lifetime of nestling ", value);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
    private void writeIntValue(PrintWriter writer, String str, int value) {
        writer.print(str);
        writer.println(value);
    }

    private void writeFloatValue(PrintWriter writer, String str, float value) {
        writer.print(str);
        writer.println(value);
    }
}

/*    public int readAdultPeriod(Scanner scanner) {
        System.out.println(scanner.next());
        System.out.println(scanner.next());
        System.out.println(scanner.next());
        return scanner.nextInt();
    }*/

/*    public int readNestlingPeriod(Scanner scanner) {
        return 0;
    }

    public int readAdultChance(Scanner scanner) {
        return 0;
    }

    public int readNestlingPercent(Scanner scanner) {
        return 0;
    }

    public int readAdultLifetime(Scanner scanner) {
        return 0;
    }

    public int readNestlingLifetime(Scanner scanner) {
        return 0;
    }*/

//        try (Scanner scanner = new Scanner(configFile)) {
//            int adultPeriod = readAdultPeriod(scanner);
//            habitat.setPeriodOfAdult(adultPeriod);
//            int nestlingPeriod = readNestlingPeriod(scanner);
//            habitat.setPeriodOfNestling(nestlingPeriod);
//
//            int adultChance = readAdultChance(scanner);
//            habitat.setChanceOfAdult(adultChance);
//            int nestlingPercent = readNestlingPercent(scanner);
//            habitat.setSpecifiedPercent(nestlingPercent);
//
//            int adultLifetime = readAdultLifetime(scanner);
//            AdultBird.setLifetime(adultLifetime);
//            int nestlingLifetime = readNestlingLifetime(scanner);
//            Nestling.setLifetime(nestlingLifetime);
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }
