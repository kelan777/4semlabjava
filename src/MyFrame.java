import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class MyFrame extends JFrame implements KeyListener {
    private SimulationControl simulationControl;
    private ImageComponent imageComponent;
    private Habitat habitat;

    private ControlPanel controlPanel;
    private JToolBar toolBar;
    private MyMenu myMenu;
    private JButton startButton;
    private JButton pauseButton;
    private JButton stopButton;

    public MyFrame(Habitat habitat){
        setLayout(new BorderLayout());
        this.habitat = habitat;
        imageComponent = new ImageComponent(habitat);
        this.habitat.setImageComponent(imageComponent);

        addKeyListener(this);

        setSize(habitat.getWidth(),habitat.getHeight());
        center(this);

        add(imageComponent, BorderLayout.CENTER);

        simulationControl = new SimulationControl(habitat, imageComponent, this);

        createControlPanel();
        myMenu = new MyMenu(this, simulationControl, controlPanel);

        setJMenuBar(myMenu.getMenuBar());
        createJToolBar();

        setFocusable(true);
    }

    @Override
    public void keyPressed(KeyEvent e) {
        //key code of 'E' is 69
        if(e.getKeyCode() == 69){
            simulationControl.pause();
            createMessageDialog();
        }
        //key code of 'B' is 66
        if(e.getKeyCode() == 66){
            simulationControl.start();
            System.out.println("B!");
        }
        //key code of 'T' is 84
        if(e.getKeyCode() == 84) {
            simulationControl.showTime();
        }
        //key code of 'Q' is 81
        if(e.getKeyCode() == 81) {
            simulationControl.finish();
        }
    }
    @Override
    public void keyReleased(KeyEvent e) {    }
    @Override
    public void keyTyped(KeyEvent e) {    }

    protected void center(Component component) {
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        component.setLocation(dim.width/2-component.getSize().width/2,
                dim.height/2-component.getSize().height/2);
    }

    protected void createMessageDialog() {
        if(controlPanel.isAllowed()) {
            MessageDialog dialog = new MessageDialog(this, simulationControl,
                    createResultString(AdultBird.getCount(), Nestling.getCount(),
                            simulationControl.getDuration()));
            dialog.setVisible(true);
        }
    }

    private void createJToolBar() {
        toolBar = new JToolBar();
        startButton = new JButton("Start");
        pauseButton = new JButton("Pause");
        stopButton = new JButton("Stop");
        toolBar.add(startButton);
        toolBar.add(pauseButton);
        toolBar.add(stopButton);
        pauseButton.setEnabled(false);
        stopButton.setEnabled(false);
        startButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                simulationControl.start();
            }
        });
        pauseButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                simulationControl.pause();
                createMessageDialog();
            }
        });
        stopButton.addActionListener(e -> simulationControl.finish());

        add(toolBar, BorderLayout.PAGE_START);

        //из за рамки панель инструментов не передвигается
        Border border = BorderFactory.createEtchedBorder();
        toolBar.setBorder(border);
    }

    private void createControlPanel() {
        controlPanel = new ControlPanel(this, habitat, simulationControl,
                habitat.getBirdsManager());
        Border border = BorderFactory.createEtchedBorder();
        controlPanel.setBorder(border);
        add(controlPanel, BorderLayout.EAST);
    }

    protected String createResultString(int numberOfAdult, int numberOfNestling, long duration) {
        return "Number of adult bird: " + numberOfAdult + ".\nNumber of nestling: "
                + numberOfNestling + ".\nDuration of simulation: " + duration + " ms.";
    }

    protected void turnOnStart() {
        controlPanel.setEnableStart(false);
        controlPanel.setEnablePause(true);
        myMenu.setEnableStart(false);
        myMenu.setEnablePause(true);
        startButton.setEnabled(false);
        pauseButton.setEnabled(true);
        stopButton.setEnabled(true);
    }

    protected void turnOnPause() {
        controlPanel.setEnablePause(false);
        controlPanel.setEnableStart(true);
        myMenu.setEnablePause(false);
        myMenu.setEnableStart(true);
        pauseButton.setEnabled(false);
        stopButton.setEnabled(false);
        startButton.setEnabled(true);
    }

    protected void turnOnShowTime(long duration, boolean isVisible) {
        controlPanel.displayDuration(duration, isVisible);
    }

    protected void turnOnFinish() {
        controlPanel.setEnablePause(false);
        controlPanel.setEnableStart(true);
        controlPanel.setSelectedShowInfo(false);
        startButton.setEnabled(true);
    }

    /*public ImageComponent getImageComponent() {
        return imageComponent;
    }*/

    public ControlPanel getControlPanel() {
        return controlPanel;
    }

    public Habitat getHabitat() {
        return habitat;
    }

    public long getDuration() {
        return simulationControl.getDuration();
    }
}
