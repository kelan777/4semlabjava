import javax.swing.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Vector;

public class ListDialog extends JDialog {
    ListDialog(MyFrame frame, Map<Bird, Long> idAndTime) {
        super(frame, "Current objects", true);
        JPanel panel = new JPanel();
        //setSize(300, 200);

        Vector<Vector<String>> vectorList = new Vector<>();

        synchronized (idAndTime) {
            for(Map.Entry<Bird, Long> entry : idAndTime.entrySet()) {
                Vector<String> stringVector = new Vector<>();
                Bird bird = entry.getKey();
                Long time = entry.getValue();
                stringVector.add(bird.getType());
                stringVector.add(String.valueOf(time));
                stringVector.add(String.valueOf(bird.getId()));
                vectorList.add(stringVector);
            }
        }


        Vector<String> names = new Vector<>();
        names.add("Type of bird");
        names.add("Time of birth (ms)");
        names.add("Identifier");

        JTable table = new JTable(vectorList, names);
        JScrollPane scrollPane = new JScrollPane(table);
        panel.add(scrollPane);
        add(panel);
        frame.center(this);
        pack();
    }

    private void spareListDialog(MyFrame frame, Map<Bird, Long> idAndTime) {
        //super(frame, "Current objects", true);
        JPanel panel = new JPanel();
        //setSize(300, 200);


        JList list = new JList();
        Vector<String> stringVector = new Vector<>();
        stringVector.add("Type       Time        ID");
        for(Map.Entry<Bird, Long> entry : idAndTime.entrySet()) {
            Bird bird = entry.getKey();
            Long time = entry.getValue();
            stringVector.add(bird.getType() + " " + time + " ms  [" + bird.getId() + "]");
        }
        list.setListData(stringVector);
        JScrollPane scrollPane = new JScrollPane(list);
        panel.add(scrollPane);
        add(panel);
        frame.center(this);
        pack();
    }
}
