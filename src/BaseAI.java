import java.awt.*;
import java.io.Serializable;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class BaseAI {
    private final List<Bird> birds;
    private ImageComponent imageComponent;
    private Dimension dimension;

    private MoveThread moveThread;
    private Boolean isStart = false;

    private Vector2f randDir = new Vector2f(1,0);
    private final int period = 7000; //ms

    public BaseAI(List<Bird> list) {
        birds = list;
    }

    public void generateRandDirection() {
        Timer timer = new Timer();
        Random random = new Random(System.currentTimeMillis());
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                randDir.setX(random.nextInt(5));
                randDir.setY(random.nextInt(5));
                if (random.nextInt() > 0)
                    randDir.setX(-randDir.getX());
                if (random.nextInt() > 0)
                    randDir.setY(-randDir.getY());
            }
        }, 0, period);
    }

    public Dimension getDimension() {
        return dimension;
    }

    public void setImageComponent(ImageComponent imageComponent) {
        this.imageComponent = imageComponent;
    }

    public void moveFlock() {
        moveThread = new MoveThread();
        moveThread.start();
    }

    public void stopFlockBehavior() {
        moveThread.stopAI();
    }
    public void startFlockBehavior() {
        moveThread.startAI();
    }

    public boolean isCreatedMoveRunnable() {
        return moveThread != null;
    }

    public boolean isStarted() {
        return isStart;
    }

    private class MoveThread extends Thread {
        @Override
        public void run() {
            isStart = true;
            generateRandDirection();

            while (true) {
                dimension = imageComponent.getSize();
                synchronized (birds) {
                    for (Bird bird : birds) {
                        bird.move();
                    }
                }
                synchronized (this) {
                    while (!isStart) {
                        try {
                            wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                            System.out.println("IN IS START LOOP");
                        }
                    }
                }
                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    System.out.println("IN SLEEP");
                }
            }
        }
        public synchronized void stopAI() {
            isStart = false;
        }

        public synchronized void startAI() {
            isStart = true;
            notify();
        }
    }
    public Vector2f separation(Bird bird) {
        Vector2f c = new Vector2f();
        synchronized (birds) {
            for (Bird b : birds) {
                if (b != bird) {
                    if(Vector2f.distance(b.getPosition(), bird.getPosition()) < 85) {
                        Vector2f vec = new Vector2f(Vector2f.getDifference(b.getPosition(),
                                bird.getPosition()));
                        if (vec.getX() == 0 && vec.getY() == 0) {
                            Random random = new Random(System.currentTimeMillis());
                            vec.setX(random.nextInt(2));
                            vec.setY(random.nextInt(2));
                        }
                        c.subtract(vec);
                    }

                }
            }
        }

        return c;
    }

    public Vector2f alignment(Bird bird) {
        Vector2f v = new Vector2f();

        synchronized (birds) {
            for (Bird b : birds) {
                if (b != bird) {
                    v.add(b.getVelocity());
                }
            }

            v.divide(birds.size() - 1);
        }

        return new Vector2f(Vector2f.getQuotient(
                Vector2f.getDifference(v, bird.getVelocity()), 1));
    }

    public Vector2f cohesion(Bird bird) {
        Vector2f center = new Vector2f();

        synchronized (birds) {
            for (Bird b : birds) {
                if (b != bird)
                    center.add(b.getPosition());
            }

            center.divide(birds.size() - 1);
        }

        center.subtract(bird.getPosition());
        center.divide(35);

        return center;
    }

    public Vector2f getRandDir() {
        return randDir;
    }

    public MoveThread getMoveThread() {
        return moveThread;
    }
}

//    public void moveBirds(ImageComponent imageComponent) {
//        dimension = imageComponent.getSize();
//        Timer timer = new Timer();
//        generateRandDirection();
//        timer.schedule(new TimerTask() {
//            @Override
//            public void run() {
//                synchronized (birds) {
//                    for (Bird bird : birds) {
//                        bird.move();
//                        //imageComponent.update();
//                    }
//                }
//            }
//        }, 0, 50);
//    }