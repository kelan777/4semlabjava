import java.util.*;

public class BirdsManager {
    private List<Bird> birds;
    private Set<Long> identifiers;
    private Map<Bird, Long> birdAndTime;
    private BaseAI behavior;

    BirdsManager(List<Bird> birds) {
        this.birds = birds;
        behavior = new BaseAI(birds);
        Bird.setBehavior(behavior);
        identifiers = new TreeSet<>();
        birdAndTime = new HashMap<>();
    }

    public void setImageComponentToBeh(ImageComponent imageComponent) {
        behavior.setImageComponent(imageComponent);
    }

    //public synchronized void add(Bird bird, long timeOfBirth) {
    public void add(Bird bird, long timeOfBirth) {
        long id = generateRandomId();
        identifiers.add(id);
        bird.setId(id);
        synchronized (birds) {
            birds.add(bird);
        }
        synchronized (birdAndTime) {
            birdAndTime.put(bird, timeOfBirth);
        }

    }

    private long generateRandomId() {
        Random random = new Random(System.currentTimeMillis());
        long result;
        for(result = random.nextLong(); identifiers.contains(result); result = random.nextLong());
        return result;
    }

    //public synchronized void check(long duration) {
    public void check(long duration) {
        synchronized (birds) {
            Iterator<Bird> it = birds.iterator();
            while (it.hasNext()){
                Bird bird = it.next();
                long tmp = bird.getTimeOfBirth() + bird.getLifetime()*1000;
                if (duration >= tmp) {
                    it.remove();
                    removeBird(bird);
                }
            }
        }
    }

    public List<Bird> getBirds() {
        return birds;
    }

    public void refresh() {
        birds.clear();
        identifiers.clear();
        synchronized (birdAndTime) {
            birdAndTime.clear();
        }
    }

    private void removeBird(Bird bird) {
        bird.decCount();
        identifiers.remove(bird.getId());
        synchronized (birdAndTime) {
            birdAndTime.remove(bird);
        }
    }

    public Map<Bird, Long> getBirdAndTime() {
        return birdAndTime;
    }

    public BaseAI getBehavior() {
        return behavior;
    }
}
