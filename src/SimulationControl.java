import javax.swing.*;
import java.io.File;
import java.time.Duration;
import java.time.Instant;
import java.util.Timer;
import java.util.TimerTask;

public class SimulationControl {
    //stopSimulation and isReadyStart are needed for organization of subsequent simulations
    private boolean isPauseSimulation = true;
    private boolean isReadyStart = true;

    private Instant start = null;
    private Timer simulationTimer = new Timer();

    //needed for display duration of simulation
    private boolean isVisible = false;
    //in ms
    private long duration = 0;
    private long prevDuration;

    private Timer timerOfCheck;

    private Habitat habitat;
    private ImageComponent imageComponent;
    private MyFrame frame;

    //true if FlockBehavior was stopped by pause() method; else false
    private boolean isStart = false;

    private ConfigManager configManager;

    private boolean isFinish = true;

    SimulationControl(Habitat habitat, ImageComponent imageComponent, MyFrame frame)
    {
        this.habitat = habitat;
        this.imageComponent = imageComponent;
        this.frame = frame;
        configManager = new ConfigManager(new File("/home/dima/IdeaProjects/BirdsLab/config.txt"),
                habitat);
        configManager.read();
    }

    protected void start() {
        if(isPauseSimulation) {
            if(start == null || isReadyStart) {
                start = Instant.now();
                isReadyStart = false;

            }
            if (isStart) {
                if (habitat.getFlockBehavior().getMoveThread() != null)
                    habitat.getFlockBehavior().startFlockBehavior();
                isStart = false;
            }
            if (isFinish) {
                isFinish = false;
                habitat.update(this);
            }

            boolean statusStartAI = frame.getControlPanel().isStatusStartAI();
            frame.getControlPanel().setEnableStartAI(statusStartAI);
            boolean statusStopAI = frame.getControlPanel().isStatusStopAI();
            frame.getControlPanel().setEnableStopAI(statusStopAI);

            ///habitat.update(this);

            timerOfCheck = new Timer();
            timerOfCheck.schedule(new TimerTask() {
                @Override
                public void run() {
                    measureDuration();
                    habitat.getBirdsManager().check(duration);
                    imageComponent.update();
                }
            }, 0,100);

            isPauseSimulation = false;
            frame.turnOnStart();
        }
    }

    protected void pause() {
        habitat.getTimer().cancel();
        timerOfCheck.cancel();
        System.out.println("E!");
        measureDuration();
        isPauseSimulation = true;
        start = null;
        prevDuration = duration;
        frame.turnOnPause();

        if (habitat.getFlockBehavior().getMoveThread() != null)
            habitat.getFlockBehavior().stopFlockBehavior();

        if (frame.getControlPanel().isStatusStopAI())
            isStart = true;


        frame.getControlPanel().setEnableStartAI(false);
        frame.getControlPanel().setEnableStopAI(false);
    }

    protected void showTime() {
        System.out.println("T!");
        if (isVisible)
            isVisible = false;
        else
            isVisible = true;

        simulationTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                if(!isPauseSimulation) {
                    measureDuration();
                }
                frame.turnOnShowTime(duration, isVisible);
            }
        }, 0, 1);
    }

    protected void finish() {
        if(!isReadyStart) {
            timerOfCheck.cancel();
            if(!isPauseSimulation) {
                habitat.getTimer().cancel();
            }
            measureDuration();
            String message = frame.createResultString(AdultBird.getCount(),Nestling.getCount(),
                    duration);
            JOptionPane.showMessageDialog(frame, message,
                    "Simulation result", JOptionPane.INFORMATION_MESSAGE);
            isPauseSimulation = true;
            isReadyStart = true;
            prevDuration = 0;
            habitat.refresh();
            imageComponent.update();

            frame.turnOnFinish();
            //habitat.getFlockBehavior().stopFlockBehavior();
            if (habitat.getFlockBehavior().getMoveThread() != null) {
                habitat.getFlockBehavior().stopFlockBehavior();
                frame.getControlPanel().setEnableStartAI(true);
                frame.getControlPanel().setEnableStopAI(false);
            }
            configManager.write();
            isFinish = true;
        }
    }

    protected synchronized long getDuration() {
        return duration;
    }

    private void measureDuration() {
        if(start == null)
            return;
        Instant end = Instant.now();
        Duration timeElapsed = Duration.between(start, end);
        duration = timeElapsed.toMillis() + prevDuration;
    }

    protected boolean getIsVisible() { return isVisible; }
}
