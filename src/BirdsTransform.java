import java.io.*;
import java.util.List;
import java.util.Map;

public class BirdsTransform {
    public void saveAll(List<Bird> birds, File file) {
        try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(file))) {
            synchronized (birds) {
                out.writeObject(birds);
            }
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }

    public void readAll(BirdsManager birdsManager, File file, long duration) {
        List<Bird> birds = birdsManager.getBirds();
        List<Bird> birdList = null;
        try (ObjectInputStream in = new ObjectInputStream(new FileInputStream(file))) {
            try {
                birdList = (List<Bird>) in.readObject();
            } catch (ClassNotFoundException e3)  {
                e3.printStackTrace();
            }
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }

        if (birdList != null) {

            Map<Bird, Long> idAndTime = birdsManager.getBirdAndTime();
            for (Bird bird : birdList) {
                if (bird instanceof AdultBird) {
                    int adultCount = AdultBird.getCount();
                    AdultBird.setCount(adultCount + 1);
                } else {
                    int nestlingCount = Nestling.getCount();
                    Nestling.setCount(nestlingCount + 1);
                }
                bird.setTimeOfBirth(duration);

                synchronized (idAndTime) {
                    idAndTime.put(bird, bird.getTimeOfBirth());
                }
            }

            synchronized (birds) {
                birds.addAll(birdList);
            }
        }
    }
}
