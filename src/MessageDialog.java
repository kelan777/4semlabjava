import javax.swing.*;
import java.awt.*;

public class MessageDialog extends JDialog {

    MessageDialog(MyFrame frame, SimulationControl simulationControl, String result) {
        super(frame, "Result of simulation", true);
        JPanel panel = new JPanel();

        JButton okButton = new JButton("OK");
        okButton.addActionListener(e -> {
                setVisible(false);
                simulationControl.finish();
            });
        JButton cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(e -> {
                setVisible(false);
                simulationControl.start();
        });

        TextArea textArea = new TextArea(result);
        textArea.setEditable(false);

        panel.add(okButton);
        panel.add(cancelButton);
        add(textArea);
        add(panel, BorderLayout.SOUTH);

        setSize(300, 200);
        frame.center(this);
    }
}
