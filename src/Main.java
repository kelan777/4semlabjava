import java.awt.*;
import java.io.*;
import java.util.LinkedList;
import javax.swing.*;
import java.util.List;
import java.util.Random;

public class Main {

    public static void mains(String[] args) {


    }

    public static void main(String[] args)
    {
        List<Bird> birds = new LinkedList<>();
        AdultBird adultBird = new AdultBird();
        //adultBird.setVelocity(new Vector2f(100, 20));
        adultBird.setPosition(new Vector2f(100, 20));
        birds.add(adultBird);
        Nestling nestling = new Nestling();
        //nestling.setVelocity(new Vector2f(110, 20));
        nestling.setPosition(new Vector2f(110, 20));
        birds.add(nestling);
        BaseAI beh = new BaseAI(birds);
        AdultBird newBird = new AdultBird();
        //birds.add(newBird);
        //Vector2f coh = beh.cohesion(newBird);
        //Vector2f alig = beh.alignment(newBird);
        //Vector2f sep1 = beh.separation(nestling);
        //Vector2f sep2 = beh.separation(adultBird);


        Habitat habitat = new Habitat();
        habitat.setChanceOfAdult(1);
        habitat.setPeriodOfAdult(5);
        habitat.setPeriodOfNestling(5);
        habitat.setSpecifiedPercent(100);

        habitat.setWidth(1000);
        habitat.setHeight(700);

        EventQueue.invokeLater(() ->
        {
            MyFrame frame = new MyFrame(habitat);
            frame.setTitle("Bird");
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.setVisible(true);
        });
    }
}