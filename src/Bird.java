import java.awt.*;
import java.io.Serializable;
import java.util.Timer;
import java.util.TimerTask;

public abstract class Bird implements IBehaviour, Serializable {
    transient private long timeOfBirth;
    private long id;

    private Vector2f position;
    private Vector2f velocity;
    //private Vector2f acceleration;
    private final int limitSpeed = 3;

    private static BaseAI behavior;


    public static void setBehavior(BaseAI beh) {
        behavior = beh;
    }

    public Bird() {
        position = new Vector2f();
        velocity = new Vector2f();
        //acceleration = new Vector2f();
    }

    protected void limitVelocity() {
        if (velocity.length() > limitSpeed) {
            velocity.divide(velocity.length());
            velocity.multiply(limitSpeed);
        }
    }

    @Override
    public void move() {
        Vector2f randDir = behavior.getRandDir();

        Vector2f position;
        Vector2f separate;
        Vector2f dir;
        position = new Vector2f(behavior.cohesion(this));
        separate = new Vector2f(behavior.separation(this));
        dir = new Vector2f(behavior.alignment(this));

        getVelocity().add(randDir);
        getVelocity().add(position);
        getVelocity().add(separate);
        //getVelocity().add(dir);

        //getVelocity().divide(900);
        limitVelocity();
        getPosition().add(getVelocity());
        keepInBounds();
        int a = 4;
    }

    private void keepInBounds()
  /* Adjust the boid's position and velocity so it stays
     within the volume of space defined by MIN_PT and MAX_PT.
     Also check if perching should be started.
  */
    {
        Vector2f MAX_PT = new Vector2f(behavior.getDimension().width, behavior.getDimension().height);
        // check if x part of the boid's position is within the volume
        if (position.getX() > MAX_PT.getX()) {     // beyond max boundary
            position.setX(MAX_PT.getX());         // put back at edge
            velocity.setX(-velocity.getX());   // move away from boundary
        }
        else if (position.getX() < 0) {
            position.setX(1);
            velocity.setX(-(velocity.getX()));
        }


        // check if y part is within the volume
        if (position.getY() > MAX_PT.getY()) {
            position.setY(MAX_PT.getY());
            velocity.setY(-velocity.getY());
        }
        else if (position.getY() < 0) {
            position.setY(1);
            velocity.setY(-velocity.getY());
        }
    } // end of keepInBounds()

    public long getTimeOfBirth() {
        return timeOfBirth;
    }

    public void setTimeOfBirth(long timeOfBirth) {
        this.timeOfBirth = timeOfBirth;
    }

    @Override
    public int getX() {
        return (int)position.getX();
    }

    @Override
    public int getY() {
        return (int)position.getY();
    }

    @Override
    public void setX(int x) {
        position.setX(x);
    }

    @Override
    public void setY(int y) {
        position.setY(y);
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    abstract public Image getImage();
    abstract public int getLifetime();
    abstract public void decCount();
    abstract public String getType();

    public Vector2f getPosition() {
        return position;
    }

    public Vector2f getVelocity() {
        return velocity;
    }

//    public Vector2f getAcceleration() {
//        return acceleration;
//    }

    public void setVelocity(Vector2f velocity) {
        this.velocity = velocity;
    }

    public void setPosition(Vector2f position) {
        this.position = position;
    }

//    public void setAcceleration(Vector2f acceleration) {
//        this.acceleration = acceleration;
//    }
}
