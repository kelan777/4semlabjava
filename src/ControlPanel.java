import javax.swing.*;
import javax.swing.text.JTextComponent;
import java.awt.*;
import java.awt.event.*;

public class ControlPanel extends JPanel {
    private static final int DEFAULT_WIDTH = 400;
    private static final int DEFAULT_HEIGHT = 400;

    private JButton startButton = new JButton("Start");
    private JButton pauseButton = new JButton("Pause");
    private ButtonGroup group = new ButtonGroup();
    private JRadioButton showTime = new JRadioButton("Show the simulation time");
    private JRadioButton hideTime = new JRadioButton("Hide the simulation time", true);
    private JCheckBox showInfo = new JCheckBox("Show info");
    private JButton currentObjects = new JButton("Current objects");
    private boolean isAllowed = false;

    private JTextField periodAdultText;
    private JTextField periodNestlingText;

    private JTextField lifetimeAdultText;
    private JTextField lifetimeNestlingText;

    private JSlider slider;

    private MyFrame frame;
    private Habitat habitat;

    private JComboBox<Float> chanceBox;

    private JTextField durationText;

    private SimulationControl simulationControl;
    private BirdsManager birdsManager;

    private JButton startAI = new JButton("start AI");
    private boolean statusStartAI = true;
    private JButton stopAI = new JButton("stop AI");
    private boolean statusStopAI = false;

    public ControlPanel(MyFrame frame, Habitat habitat, SimulationControl simulationControl,
                        BirdsManager birdsManager) {
        this.frame = frame;
        this.habitat = habitat;
        this.simulationControl = simulationControl;
        this.birdsManager = birdsManager;

        setBackground(Color.PINK);
        Dimension dimension = new Dimension(DEFAULT_WIDTH, DEFAULT_HEIGHT);
        setPreferredSize(dimension);

        add(startButton);
        pauseButton.setEnabled(false);
        add(pauseButton);
        pauseButton.setEnabled(false);
        add(showInfo);
        durationText = new JTextField(11);
        durationText.setEditable(false);
        add(durationText);
        add(currentObjects);

        placeTimeButtons();

        createInputPeriods();
        placeInputPeriod();

        createInputChance();
        placeInputChance();

        createInputPercent();
        placeInputPercent();

        createInputLifetime();
        placeInputLifetime();

        createAIButtons();

        addListeners();
    }

    private void createAIButtons() {
        stopAI.setEnabled(false);
        BaseAI beh = birdsManager.getBehavior();
        startAI.addActionListener(e -> {
            if (!beh.isCreatedMoveRunnable())
                beh.moveFlock();
            else
                beh.startFlockBehavior();
            stopAI.setEnabled(true);
            statusStopAI = true;
            startAI.setEnabled(false);
            statusStartAI = false;
        });
        stopAI.addActionListener(e -> {
            beh.stopFlockBehavior();
            startAI.setEnabled(true);
            statusStartAI = true;
            stopAI.setEnabled(false);
            statusStopAI = false;
        });

        JPanel panel = new JPanel();

        GroupLayout layout = new GroupLayout(panel);
        panel.setLayout(layout);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);

        layout.setHorizontalGroup(layout.createParallelGroup().
                addComponent(startAI).
                addComponent(stopAI));

        layout.setVerticalGroup(layout.createSequentialGroup().
                addComponent(startAI).addComponent(stopAI));

        add(panel);
    }

    private void placeTimeButtons() {
        JPanel panel = new JPanel();

        GroupLayout layout = new GroupLayout(panel);
        panel.setLayout(layout);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);

        layout.setHorizontalGroup(layout.createParallelGroup().addComponent(showTime).
                addComponent(hideTime));

        layout.setVerticalGroup(layout.createSequentialGroup().
                addComponent(showTime).addComponent(hideTime));

        group.add(showTime);
        group.add(hideTime);
        add(panel);
    }

    private void createInputPeriods() {
        periodAdultText = new JTextField(5);
        periodAdultText.setText(String.valueOf(habitat.getPeriodOfAdult()));
        TextFocusListener textListener = new TextFocusListener(periodAdultText, Type.ADULT_PERIOD);
        periodAdultText.addFocusListener(textListener);

        periodNestlingText = new JTextField(5);
        periodNestlingText.setText(String.valueOf(habitat.getPeriodOfNestling()));
        textListener = new TextFocusListener(periodNestlingText, Type.NESTLING_PERIOD);
        periodNestlingText.addFocusListener(textListener);
    }

    private void createInputChance() {
        chanceBox = new JComboBox<>();
        chanceBox.addItem(0f);
        chanceBox.addItem(0.1f);
        chanceBox.addItem(0.2f);
        chanceBox.addItem(0.3f);
        chanceBox.addItem(0.4f);
        chanceBox.addItem(0.5f);
        chanceBox.addItem(0.6f);
        chanceBox.addItem(0.7f);
        chanceBox.addItem(0.8f);
        chanceBox.addItem(0.9f);
        chanceBox.addItem(1f);
        chanceBox.setSelectedItem(habitat.getChanceOfAdult());

        chanceBox.addActionListener(e -> {
            float select = chanceBox.getItemAt(chanceBox.getSelectedIndex());
            habitat.setChanceOfAdult(select);
            System.out.println(select);
        });
    }

    private void placeInputChance() {
        JPanel panel = new JPanel();

        GroupLayout layout = new GroupLayout(panel);
        panel.setLayout(layout);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);

        JLabel labelAdult = new JLabel("Chances of birdbirth: ", JLabel.RIGHT);
        layout.setHorizontalGroup(layout.createSequentialGroup().
                addComponent(labelAdult).
                addComponent(chanceBox));

        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER).
                addComponent(labelAdult).addComponent(chanceBox));

        add(panel);
    }

    private void addListeners() {
        startButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.requestFocus();
                simulationControl.start();
            }
        });

        pauseButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.requestFocus();
                simulationControl.pause();
                frame.createMessageDialog();
            }
        });

        showTime.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.requestFocus();
                if (!simulationControl.getIsVisible())
                    simulationControl.showTime();
            }
        });

        hideTime.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.requestFocus();
                if (simulationControl.getIsVisible())
                    simulationControl.showTime();
            }
        });

        showInfo.addItemListener(e -> {
            frame.requestFocus();
            if (showInfo.isSelected())
                isAllowed = true;
            else
                isAllowed = false;
        });

        currentObjects.addActionListener(e -> new ListDialog(frame, birdsManager.getBirdAndTime()).
                setVisible(true));
    }

    protected void setEnableStart(boolean b) {
        startButton.setEnabled(b);
    }

    protected void setEnablePause(boolean b) {
        pauseButton.setEnabled(b);
    }

    protected void setEnableStartAI(boolean b) {
        startAI.setEnabled(b);
    }
    protected void setEnableStopAI(boolean b) {
        stopAI.setEnabled(b);
    }

    protected boolean isAllowed() {
        return isAllowed;
    }

    protected void setSelectedShowInfo(boolean b) {
        showInfo.setSelected(b);
    }

    private void placeInputPeriod() {
        JPanel panel = new JPanel();

        GroupLayout layout = new GroupLayout(panel);
        panel.setLayout(layout);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);

        JLabel labelAdult = new JLabel("Period of adult bird: ", JLabel.RIGHT);
        JLabel labelNestling = new JLabel("Period of nestling: ", JLabel.RIGHT);

        layout.setHorizontalGroup(layout.createSequentialGroup().
                addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).
                        addComponent(labelAdult).addComponent(labelNestling)).
                addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).
                        addComponent(periodAdultText).addComponent(periodNestlingText)));

        layout.setVerticalGroup(layout.createSequentialGroup().addGroup(
                layout.createParallelGroup().addComponent(labelAdult).
                        addComponent(periodAdultText)).
                addGroup(layout.createParallelGroup().
                        addComponent(labelNestling).addComponent(periodNestlingText)));

        add(panel);
    }

    private void createInputPercent() {
        slider = new JSlider(JSlider.HORIZONTAL, 0, 100, habitat.getSpecifiedPercent());

        slider.setMajorTickSpacing(20);
        slider.setMinorTickSpacing(10);
        slider.setSnapToTicks(true);
        slider.setPaintTicks(true);
        slider.setPaintLabels(true);
        slider.addChangeListener(e -> {
            System.out.println("");
            System.out.println("Slider Listener in createInputPercent:" + slider.getValue());
            habitat.setSpecifiedPercent(slider.getValue());
        });
    }

    private void placeInputPercent() {
        JPanel panel = new JPanel();

        GroupLayout layout = new GroupLayout(panel);
        panel.setLayout(layout);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);

        JLabel labelNestling = new JLabel("Percent of birdbirth: ", JLabel.RIGHT);
        layout.setHorizontalGroup(layout.createSequentialGroup().
                addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).
                        addComponent(labelNestling)).
                addComponent(slider));

        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).
                addGroup(layout.createSequentialGroup().addComponent(labelNestling)).addComponent(slider));

        add(panel);
    }

    protected void displayDuration(long duration, boolean isVisible) {
        if (isVisible)
            durationText.setText("Time: " + duration);
        else
            durationText.setText("");
    }

    private void createInputLifetime() {
        lifetimeAdultText = new JTextField(4);
        lifetimeAdultText.setText(String.valueOf(AdultBird.getLifeTime()));
        TextFocusListener textListener = new TextFocusListener(lifetimeAdultText, Type.ADULT_LIFE);
        lifetimeAdultText.addFocusListener(textListener);

        lifetimeNestlingText = new JTextField(4);
        lifetimeNestlingText.setText(String.valueOf(Nestling.getLifeTime()));
        textListener = new TextFocusListener(lifetimeNestlingText, Type.NESTLING_LIFE);
        lifetimeNestlingText.addFocusListener(textListener);
    }

    enum Type { ADULT_PERIOD, NESTLING_PERIOD, ADULT_LIFE, NESTLING_LIFE }


    private class TextFocusListener extends FocusAdapter {
        JTextComponent textComponent;
        Type type;

        TextFocusListener(JTextComponent textComponent, Type type) {
            this.textComponent = textComponent;
            this.type = type;
        }

        private void setValue(int value) {
            if (type == Type.ADULT_PERIOD) {
                habitat.setPeriodOfAdult(value);
            } else if (type == Type.NESTLING_PERIOD) {
                habitat.setPeriodOfNestling(value);
            } else if (type == Type.ADULT_LIFE) {
                AdultBird.setLifetime(value);
            } else if (type == Type.NESTLING_LIFE) {
                Nestling.setLifetime(value);
            }
        }

        @Override
        public void focusLost(FocusEvent e) {
            String inputStr = textComponent.getText();

            if (inputStr.equals(""))
                return;

            int input = 0;
            try {
                input = Integer.parseInt(inputStr);
                if (input <= 0)
                    throw new IllegalArgumentException("Non-positive input");
            } catch (NumberFormatException exc) {
                JOptionPane.showMessageDialog(frame,
                        "Invalid user input. Please enter a number", "Error",
                        JOptionPane.ERROR_MESSAGE);
                return;
            } catch (IllegalArgumentException exp2) {
                JOptionPane.showMessageDialog(frame,
                        "Invalid user input. Please enter a positive number", "Error",
                        JOptionPane.ERROR_MESSAGE);
                return;
            }
            System.out.println(input);
            setValue(input);
        }
    }

    private void placeInputLifetime() {
        JPanel panel = new JPanel();

        GroupLayout layout = new GroupLayout(panel);
        panel.setLayout(layout);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);

        JLabel labelAdult = new JLabel("Lifetime of adult bird: ", JLabel.RIGHT);
        JLabel labelNestling = new JLabel("Lifetime of nestling: ", JLabel.RIGHT);
        layout.setHorizontalGroup(layout.createSequentialGroup().
                addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).
                        addComponent(labelAdult).addComponent(labelNestling)).
                addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING).
                        addComponent(lifetimeAdultText).addComponent(lifetimeNestlingText)));

        layout.setVerticalGroup(layout.createSequentialGroup().addGroup(
                layout.createParallelGroup().
                        addComponent(labelAdult).addComponent(lifetimeAdultText)).
                addGroup(layout.createParallelGroup().
                        addComponent(labelNestling).addComponent(lifetimeNestlingText)));

        add(panel);
    }

    public boolean isStatusStartAI() {
        return statusStartAI;
    }

    public boolean isStatusStopAI() {
        return statusStopAI;
    }

    public void changePercentChicks(int value) {
        habitat.setSpecifiedPercent(value);
        slider.setValue(value);
    }
}
