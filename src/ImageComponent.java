import javax.swing.*;
import java.awt.*;
import java.util.*;
import java.util.List;

public class ImageComponent extends JComponent {
    private Habitat habitat;
    private List<Bird> list;

    ImageComponent(Habitat habitat)
    {
        this.habitat = habitat;
        list = new LinkedList<>(habitat.getList());
    }

    @Override
    public void paintComponent(Graphics g)
    {
        super.paintComponent(g);
        Iterator<Bird> it = list.iterator();
        while(it.hasNext()) {
            Bird p = it.next();
            if(p == null) {
                System.out.println("NULL BIRD");
                it.remove();
            }
            g.drawImage(p.getImage(), p.getX(), p.getY(), null);
        }
    }

    public void update()
    {
        list = new LinkedList<>(habitat.getList());
        repaint();
    }
}
