import java.awt.*;

public class AdultBird extends Bird {
    private static Image image;

    private static int lifetime = 2;
    private static int count = 0;

    public AdultBird() {
        count++;
    }

    @Override
    public Image getImage() { return image; }

    public static void setImage(Image newImage) { image = newImage; }

    public static void setLifetime(int lifetime) {
        AdultBird.lifetime = lifetime;
    }

    public int getLifetime() {
        return lifetime;
    }

    public static int getLifeTime() {
        return lifetime;
    }

    public void decCount() {
        count--;
    }

    public static int getCount() {
        return count;
    }

    public static void setCount(int count) {
        AdultBird.count = count;
    }

    @Override
    public String getType() {
        return "Adult bird";
    }
}
