import javax.swing.*;

public class MessageBox {

    MessageBox(MyFrame frame, SimulationControl simulationControl, String result) {
        int select = JOptionPane.showConfirmDialog(frame, result, "My dialog",
                JOptionPane.OK_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE);
        if(select == JOptionPane.OK_OPTION)
            simulationControl.start();
        else if(select == JOptionPane.CANCEL_OPTION)
            simulationControl.finish();
    }
}
