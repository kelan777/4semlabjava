import javax.swing.*;
import java.awt.*;
import java.util.*;
import java.util.List;
import java.util.Timer;

public class Habitat {
    private int periodOfAdult;
    private float chanceOfAdult;

    private int specifiedPercent;
    private int periodOfNestling;

    private int width = 800;
    private int height = 600;

    private Image imageNestling = new ImageIcon("chicks.jpg").getImage();
    private Image imageAdult = new ImageIcon("bird.jpg").getImage();

    private Timer timer;
    private List<Bird> birds;
    private ImageComponent imageComponent;

    private BirdsManager birdsManager;

    public Habitat() {
        AdultBird.setImage(imageAdult);
        Nestling.setImage(imageNestling);
        birds = new LinkedList<>();
        birdsManager = new BirdsManager(birds);
    }

    //arguments is started time from simulation
    public void update(SimulationControl simulationControl)
    {
        timer = new Timer();
        //add Adult Bird
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                if(chanceOfAdult != 0.0f && chanceOfAdult >= (float)Math.random()) {
                    long duration = simulationControl.getDuration();
                    AdultBird adultBird = new AdultBird();
                    adultBird.setTimeOfBirth(duration);
                    setRandomCoordinate(adultBird);
                    birdsManager.add(adultBird, duration);
                    imageComponent.update();
                    birds = birdsManager.getBirds();
                }
            }
        }, 0, periodOfAdult*1000);

        //add Nestling
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                if(getPermissionToCreate()) {
                    long duration = simulationControl.getDuration();
                    Nestling nestling = new Nestling();
                    nestling.setTimeOfBirth(duration);
                    setRandomCoordinate(nestling);
                    birdsManager.add(nestling, duration);
                    imageComponent.update();
                    birds = birdsManager.getBirds();
                }
            }
        }, 0, periodOfNestling*1000);
    }

    private boolean getPermissionToCreate() {
        if(specifiedPercent == 0)
            return false;
        if (Nestling.getCount() == 0) {
            if(AdultBird.getCount() >= 2)
                return true;
            else
                return false;
        }
        return (float)Nestling.getCount()/(float)AdultBird.getCount()*100 < specifiedPercent;
    }

    private void setRandomCoordinate(Bird bird) {
        Random random = new Random(System.currentTimeMillis());
        bird.setX(random.nextInt(imageComponent.getWidth()));
        bird.setY(random.nextInt(imageComponent.getHeight()));
    }

    public Timer getTimer() {
        return timer;
    }


    public void setSpecifiedPercent(int specifiedPercent) {
        this.specifiedPercent = specifiedPercent;
    }

    public int getSpecifiedPercent() {
        return specifiedPercent;
    }

    public void setChanceOfAdult(float chanceOfAdult) {
        this.chanceOfAdult = chanceOfAdult;
    }

    public float getChanceOfAdult() {
        return chanceOfAdult;
    }

    public void setPeriodOfAdult(int periodOfAdult) {
        this.periodOfAdult = periodOfAdult;
    }

    public int getPeriodOfAdult() {
        return periodOfAdult;
    }

    public void setPeriodOfNestling(int periodOfNestling) {
        this.periodOfNestling = periodOfNestling;
    }

    public int getPeriodOfNestling() {
        return periodOfNestling;
    }

    public void setImageComponent(ImageComponent imageComponent) {
        this.imageComponent = imageComponent;
        birdsManager.setImageComponentToBeh(imageComponent);
    }


    public void refresh() {
        AdultBird.setCount(0);
        Nestling.setCount(0);
        birdsManager.refresh();
        birds = new LinkedList<>();

    }

    public int getHeight() { return height; }

    public int getWidth() { return width; }

    public void setHeight(int height) { this.height = height; }

    public void setWidth(int width) { this.width = width; }

    public List<Bird> getList() {
        synchronized (birds) {
            return birds;
        }
    }

    public BirdsManager getBirdsManager() {
        return birdsManager;
    }

    public BaseAI getFlockBehavior() { return birdsManager.getBehavior(); }
}