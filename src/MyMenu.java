import javax.swing.*;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.io.PipedReader;
import java.io.PipedWriter;
import java.util.Scanner;

import static java.awt.event.KeyEvent.VK_ENTER;

public class MyMenu extends JComponent {
    private JMenuBar menuBar;
    private MyFrame frame;
    private SimulationControl simulationControl;
    private JMenuItem startItem;
    private JMenuItem pauseItem;
    private ControlPanel controlPanel;
    private JMenuItem saveItem;
    private JMenuItem downloadItem;

    MyMenu(MyFrame frame, SimulationControl simulationControl, ControlPanel controlPanel) {
        this.frame = frame;
        this.simulationControl = simulationControl;
        this.controlPanel = controlPanel;
        menuBar = new JMenuBar();

        createApplicationMenu();
        createTimeMenu();
        createConsoleMenu();
    }

    private void createConsoleMenu() {
        JMenu consoleMenu = new JMenu("Console");
        JMenuItem item = new JMenuItem("GO!");
        item.addActionListener(e -> {
            ConsoleDialog consoleDialog = new ConsoleDialog(frame);
            consoleDialog.setVisible(true);

            ReadConsoleThread consoleThread = new ReadConsoleThread(consoleDialog.getPipedWriter().writer);
            consoleDialog.addWindowListener(consoleDialog.getPipedWriter());
            consoleThread.start();
        });
        consoleMenu.add(item);

        menuBar.add(consoleMenu);
    }

    private class ReadConsoleThread extends Thread {
        private PipedWriter writer;

        ReadConsoleThread(PipedWriter writer) {
            this.writer = writer;
        }

        private void executeCommand(String input) {
            String[] strings = { "change", "the", "percentage", "of", "chicks" };
            Scanner scanner = new Scanner(input);
            int i = 0;
            for (; i < strings.length && scanner.hasNext(); i++) {
                if (!strings[i].equals(scanner.next()))
                    break;
            }
            if (i == strings.length) {
                if (scanner.hasNextInt()) {
                    int percent = scanner.nextInt();

                    if (percent > 0 && percent < 100)
                        controlPanel.changePercentChicks(percent);
                    else
                        System.out.println("Invalid int value percent");
                }
            }
        }

        @Override
        public void run() {
            try (PipedReader reader = new PipedReader(writer)) {
                int res;

                StringBuilder builder = new StringBuilder(20);
                while ((res = reader.read()) != -1) {
                    if (res == VK_ENTER) {
                        System.out.print(builder);
                        executeCommand(builder.toString());
                        builder.setLength(0);
                    }
                    builder.append((char)res);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("ReadConsoleThread finish");
        }
    }

    private class ConsoleDialog extends JDialog {
        private EventPipedWriter pipedWriter;

        ConsoleDialog(MyFrame frame) {
            super(frame, "Console", false);
            setSize(600,300);
            JPanel panel = new JPanel();
            JTextArea textArea = new JTextArea(2,5);

            textArea.setBackground(Color.gray);
            textArea.setForeground(Color.CYAN);

            pipedWriter = new EventPipedWriter(textArea);
            textArea.addKeyListener(pipedWriter);

            panel.setLayout(new BorderLayout());
            panel.add(textArea);
            add(panel);
            frame.center(this);
        }

        private class EventPipedWriter extends KeyAdapter implements WindowListener {
            private PipedWriter writer = new PipedWriter();
            private JTextArea textArea;
            int offset = 0;
            EventPipedWriter(JTextArea textArea) {
                this.textArea = textArea;
            }

            @Override
            public void keyReleased(KeyEvent e) {
                if (e.getKeyCode() == VK_ENTER) {
                    Document document = textArea.getDocument();
                    int length = document.getLength();

                    try {
                        String text = document.getText(offset, length - offset);
                        char[] array = text.toCharArray();

                        try {
                            writer.write(array);
                            offset = length;
                        } catch (IOException e2) {
                            e2.printStackTrace();
                            System.out.println("WRITE() IN EVENT PIPED WRITER!");
                        }

                    } catch (BadLocationException e1) {
                        e1.printStackTrace();
                        System.out.println("EVENT PIPED WRITER!");
                    }
                }
            }
            @Override
            public void windowClosing(WindowEvent e) {
                try {
                    writer.close();
                } catch (IOException windowClosingExp) {
                    windowClosingExp.printStackTrace();
                }
            }
            @Override
            public void windowClosed(WindowEvent e) { }
            @Override
            public void windowActivated(WindowEvent e) { }
            @Override
            public void windowDeactivated(WindowEvent e) { }
            @Override
            public void windowDeiconified(WindowEvent e) { }
            @Override
            public void windowIconified(WindowEvent e) { }
            @Override
            public void windowOpened(WindowEvent e) { }
        }

        public EventPipedWriter getPipedWriter() {
            return pipedWriter;
        }
    }

    private void createApplicationMenu() {
        JMenu applicationMenu = new JMenu("Application");

        startItem = new JMenuItem("Start");
        startItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                simulationControl.start();
            }
        });
        pauseItem = new JMenuItem("Pause");
        pauseItem.setEnabled(false);
        pauseItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                simulationControl.pause();
                frame.createMessageDialog();
            }
        });

        saveItem = new JMenuItem("Save");
        saveItem.addActionListener(e -> {
            JFileChooser jFileChooserSave = new JFileChooser();
            jFileChooserSave.showSaveDialog(frame);
            File saveFile = jFileChooserSave.getSelectedFile();

            BirdsTransform birdsTransform = new BirdsTransform();
            birdsTransform.saveAll(frame.getHabitat().getBirdsManager().getBirds(), saveFile);
        });


        downloadItem = new JMenuItem("Download");
        downloadItem.addActionListener(e -> {
            JFileChooser jFileChooserDownload = new JFileChooser();
            jFileChooserDownload.showOpenDialog(frame);
            File downloadFile = jFileChooserDownload.getSelectedFile();

            BirdsTransform birdsTransform = new BirdsTransform();
            birdsTransform.readAll(frame.getHabitat().getBirdsManager(), downloadFile,
                    frame.getDuration());
        });


        applicationMenu.add(startItem);
        applicationMenu.add(pauseItem);
        applicationMenu.addSeparator();
        applicationMenu.add(saveItem);
        applicationMenu.add(downloadItem);

        menuBar.add(applicationMenu);
    }

    private void createTimeMenu() {
        JMenu timeMenu = new JMenu("Simulation time");

        JMenuItem showItem = new JMenuItem("Show");
        showItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!simulationControl.getIsVisible())
                    simulationControl.showTime();
            }
        });
        JMenuItem hideItem = new JMenuItem("Hide");
        hideItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (simulationControl.getIsVisible())
                    simulationControl.showTime();
            }
        });
        timeMenu.add(showItem);
        timeMenu.add(hideItem);

        menuBar.add(timeMenu);
    }

    protected JMenuBar getMenuBar() { return  menuBar; }

    protected void setEnableStart(boolean b) {
        startItem.setEnabled(b);
    }
    protected void setEnablePause(boolean b) {
        pauseItem.setEnabled(b);
    }

    public JMenuItem getSaveItem() {
        return saveItem;
    }
    public JMenuItem getDownloadItem() {
        return downloadItem;
    }
}
