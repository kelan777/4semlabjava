import java.awt.*;

public class Nestling extends Bird {
    private static Image image;

    private static int lifetime = 10;
    private static int count = 0;

    public Nestling() {
        count++;
    }

    @Override
    public Image getImage() { return image; }

    public static void setImage(Image newImage) { image = newImage; }

    public static void setLifetime(int lifetime) {
        Nestling.lifetime = lifetime;
    }

    public int getLifetime() {
        return lifetime;
    }

    public static int getLifeTime() { return  lifetime; }

    public static int getCount() {
        return count;
    }

    public void decCount() {
        count--;
    }

    public static void setCount(int count) {
        Nestling.count = count;
    }

    @Override
    public String getType() {
        return "Nestling";
    }
}
